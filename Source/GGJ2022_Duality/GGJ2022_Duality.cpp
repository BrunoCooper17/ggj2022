// Copyright Epic Games, Inc. All Rights Reserved.

#include "GGJ2022_Duality.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GGJ2022_Duality, "GGJ2022_Duality" );
 