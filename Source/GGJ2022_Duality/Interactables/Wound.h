﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Wound.generated.h"

class USceneComponent;
class UStaticMeshComponent;

UCLASS()
class GGJ2022_DUALITY_API AWound final : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWound();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintNativeEvent)
	void HealWound();

	bool IsHealed() const { return bHealed; }
	virtual void OnConstruction(const FTransform& Transform) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "True"))
	USceneComponent* SceneRoot;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "True"))
	UStaticMeshComponent* WoundMesh;

	bool bHealed;
};
