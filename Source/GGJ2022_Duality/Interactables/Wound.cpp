﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Wound.h"

#include "Components/BoxComponent.h"


// Sets default values
AWound::AWound() :
	bHealed{false}
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = SceneRoot;
	WoundMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wound"));
	WoundMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	WoundMesh->SetCollisionProfileName(FName("WOUND"));
	WoundMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWound::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWound::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
}

void AWound::HealWound_Implementation()
{
	bHealed = true;
	WoundMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}