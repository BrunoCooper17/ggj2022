// Copyright Epic Games, Inc. All Rights Reserved.

#include "GGJ2022_DualityGameMode.h"

#include "GameFramework/GameStateBase.h"
#include "GGJ2022_Duality/Character/GGJ2022_DualityCharacter.h"
#include "GGJ2022_Duality/HUD/PlayerHUD.h"
#include "GGJ2022_Duality/HUD/BaseUserWidget.h"
#include "GGJ2022_Duality/Interactables/Wound.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Sound/AmbientSound.h"
#include "UObject/ConstructorHelpers.h"

DECLARE_LOG_CATEGORY_CLASS(LogDualityGameMode, All, All)

AGGJ2022_DualityGameMode::AGGJ2022_DualityGameMode() :
	TimeRound{300},
	TimeElapsed{0},
	StartNarrative{nullptr},
	FadeWidget{nullptr},
	FadeWidget_Ref{nullptr},
	PlayerPawn{nullptr},
	PlayerController{nullptr},
	PlayerHUD{nullptr},
	GamePhase{Waiting},
	NextLevel{""}
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(
		TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	HUDClass = APlayerHUD::StaticClass();
}

void AGGJ2022_DualityGameMode::BeginPlay()
{
	Super::BeginPlay();

	PlayerPawn = Cast<AGGJ2022_DualityCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	PlayerHUD = Cast<APlayerHUD>(PlayerController->GetHUD());

	if (!IsValid(StartNarrative) || !IsValid(FadeWidget))
	{
		UE_LOG(LogDualityGameMode, Error, TEXT("Missing StartNarrative or FadeWidgets"))
		return;
	}

	FadeWidget_Ref = CreateWidget<UBaseUserWidget>(PlayerController, FadeWidget);
	UpdateGamePhase(EGamePhase::Intro);
}

void AGGJ2022_DualityGameMode::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	TimeElapsed = FMath::Min(TimeElapsed + DeltaSeconds, TimeRound);
	PlayerHUD->UpdateTime((TimeRound - TimeElapsed) / TimeRound);
	if (TimeElapsed >= TimeRound)
	{
		UpdateGamePhase(EGamePhase::GameOver);
	}
}

void AGGJ2022_DualityGameMode::UpdateGamePhase(const EGamePhase NewGamePhase)
{
	if (GamePhase == NewGamePhase)
		return;
	GamePhase = NewGamePhase;

	switch (GamePhase)
	{
	case Waiting: break;

	case Intro:
		{
			// Play Intro
			auto* TmpStartNarrative{CreateWidget<UBaseUserWidget>(PlayerController, StartNarrative)};
			TmpStartNarrative->AddToViewport();
			TmpStartNarrative->Show();

			PlayerPawn->UnPossessed();
			break;
		}

	case Gameloop:
		// How many wounds are in the level?
		UGameplayStatics::GetAllActorsOfClass(this, AWound::StaticClass(), WoundActors);
		PlayerHUD->DisplayHud();
		PlayerHUD->UpdateWoundCounter(WoundActors.Num());

		PlayerController->Possess(PlayerPawn);
		SetActorTickEnabled(true);
		break;

	case GameOver:
		NextLevel = "GameOverLevel";
		UpdateGamePhase(EGamePhase::Cleanup);
		break;

	case Ending:
		NextLevel = "YoureWinnerLevel";
		UpdateGamePhase(EGamePhase::Cleanup);

		break;

	case Cleanup:
		SetActorTickEnabled(false);
		UKismetSystemLibrary::Delay(this, 1.5f, FLatentActionInfo(0, FMath::Rand(), TEXT("MoveNextLevel"), this));
		Cast<AAmbientSound>(UGameplayStatics::GetActorOfClass(this, AAmbientSound::StaticClass()))->FadeOut(1.f, 0.f);
		PlayerHUD->HideHud();
		FadeWidget_Ref->AddToViewport();
		FadeWidget_Ref->Show();
		PlayerPawn->UnPossessed();
		break;

	default: ;
	}
}

void AGGJ2022_DualityGameMode::UpdateWoundsHealCounter()
{
	int8 WoundsLeft{0};
	for (auto* WoundActor : WoundActors)
	{
		if (!Cast<AWound>(WoundActor)->IsHealed())
			++WoundsLeft;
	}
	PlayerHUD->UpdateWoundCounter(WoundsLeft);

	if (!WoundsLeft)
		UpdateGamePhase(EGamePhase::Ending);
}

void AGGJ2022_DualityGameMode::MoveNextLevel()
{
	UGameplayStatics::OpenLevel(this, NextLevel);
}
