// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GGJ2022_DualityGameMode.generated.h"

class AGGJ2022_DualityCharacter;
class APlayerController;
class APlayerHUD;
class UBaseUserWidget;

UENUM()
enum EGamePhase
{
	Waiting,
	Intro,
	Gameloop,
	GameOver,
	Ending,
	Cleanup
};

UCLASS(minimalapi)
class AGGJ2022_DualityGameMode final : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGGJ2022_DualityGameMode();
protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds) override;
	
	UFUNCTION(BlueprintCallable)
	void UpdateGamePhase(EGamePhase NewGamePhase);

	void UpdateWoundsHealCounter();

	UFUNCTION()
	void MoveNextLevel();

private:
	UPROPERTY(EditAnywhere, meta=(ClampMin=10))
	float TimeRound;
	float TimeElapsed;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UBaseUserWidget> StartNarrative;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UBaseUserWidget> FadeWidget;
	UPROPERTY()
	UBaseUserWidget* FadeWidget_Ref;
	
	UPROPERTY()
	AGGJ2022_DualityCharacter* PlayerPawn;
	UPROPERTY()
	APlayerController* PlayerController;
	UPROPERTY()
	APlayerHUD* PlayerHUD;
	UPROPERTY()
	TArray<AActor*> WoundActors;
	
	EGamePhase GamePhase;
	FName NextLevel;
};
