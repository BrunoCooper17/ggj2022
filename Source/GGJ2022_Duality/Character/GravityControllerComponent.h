﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GravityControllerComponent.generated.h"

class AGravityInfluence;

USTRUCT()
struct FGravityStack
{
	GENERATED_BODY()

	void AddToStack(AGravityInfluence* GravityInfluence)
	{
		Stack.Add(GravityInfluence);
	}
	
	void RemoveFromStack(AGravityInfluence* GravityInfluence)
	{
		Stack.Remove(GravityInfluence);
		Stack.Remove(GravityInfluence);
	}

	AGravityInfluence* GetMostImportant()
	{
		if (Stack.Num() > 0)
			return Stack.Last();
		return nullptr;
	}
	
private:
	UPROPERTY()
	TArray<AGravityInfluence*> Stack;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class GGJ2022_DUALITY_API UGravityControllerComponent final : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGravityControllerComponent();

private:
	UPROPERTY()
	TMap<int8, FGravityStack> MapGravityInfluences;

	UPROPERTY()
	AGravityInfluence* LastGravityInfluence;

public:
	FVector GetGravityDirection();

	UFUNCTION()
	void RegisterGravityZone(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	                         UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	                         const FHitResult& SweepResult);
	UFUNCTION()
	void UnregisterGravityZone(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	                           UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
