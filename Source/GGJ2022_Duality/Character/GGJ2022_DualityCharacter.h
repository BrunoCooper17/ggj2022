// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "DirGravity/Public/GravityCharacter.h"
#include "GGJ2022_DualityCharacter.generated.h"

class APlayerController;
class USpringArmComponent;
class UCameraComponent;
class UInputComponent;
class UGravityMovementComponent;
class UGravityControllerComponent;
class UHealerComponent;

UCLASS(config=Game)
class AGGJ2022_DualityCharacter : public AGravityCharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UHealerComponent* HealerComponent;

	UPROPERTY()
	UGravityControllerComponent* GravityControllerComponent;

	UPROPERTY()
	APlayerController* CurrentPlayerController;

	UPROPERTY(BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	FVector CurrentGravity;
	bool bUsingGamepad;
	bool bIsSinging;

	UPROPERTY(EditAnywhere, meta=(ClampMin=0))
	float GravityInterpSpeed;

public:
	AGGJ2022_DualityCharacter(const FObjectInitializer& ObjectInitializer);

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:
	/** Used to check if using mouse and keyboard or gamepad */
	void CheckInputDeviceType(const FKey KeyPressed);

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	void SingAndHeal();

	UFUNCTION()
	void EndSinging();

	void UpdateTurnDummy(float Value)
	{
	}

	/** Update Camera Orientation */
	void UpdateCameraOrientation(float DeltaSeconds);

	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	void UpdateGravityInterpolate(float DeltaSeconds) const;

	UFUNCTION(BlueprintImplementableEvent)
	void SingEffect();

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	virtual void Tick(float DeltaSeconds) override;
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void PossessedBy(AController* NewController) override;

	UFUNCTION(BlueprintCallable)
	bool IsSinging() const { return bIsSinging; }
};
