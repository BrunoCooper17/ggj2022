﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GravityControllerComponent.h"

#include "GGJ2022_Duality/GravityZones/GravityInfluence.h"


// Sets default values for this component's properties
UGravityControllerComponent::UGravityControllerComponent() :
	LastGravityInfluence{nullptr}
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

FVector UGravityControllerComponent::GetGravityDirection()
{
	TArray<int8> Priorities;
	MapGravityInfluences.GetKeys(Priorities);

	for (const int8& Priority : Priorities)
	{
		FGravityStack* TmpStack{MapGravityInfluences.Find(Priority)};
		auto* GravityInfluence{TmpStack->GetMostImportant()};
		if (IsValid(GravityInfluence))
		{
			LastGravityInfluence = GravityInfluence;
			return LastGravityInfluence->GetGravity(GetOwner()->GetActorLocation());
		}
	}
	return IsValid(LastGravityInfluence) ? LastGravityInfluence->GetGravity(GetOwner()->GetActorLocation()) : FVector::DownVector;
}

void UGravityControllerComponent::RegisterGravityZone(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                                      UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
                                                      bool bFromSweep, const FHitResult& SweepResult)
{
	auto* TmpGravityInfluence{Cast<AGravityInfluence>(OtherActor)};
	if (!IsValid(TmpGravityInfluence))
		return;
	FGravityStack& TmpStack{MapGravityInfluences.FindOrAdd(TmpGravityInfluence->GetPriority())};
	TmpStack.AddToStack(TmpGravityInfluence);
	MapGravityInfluences.KeySort([](const int32 A, const int32 B)
	{
		return A > B; // sort keys in reverse
	});
}

void UGravityControllerComponent::UnregisterGravityZone(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                                        UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	auto* TmpGravityInfluence{Cast<AGravityInfluence>(OtherActor)};
	if (!IsValid(TmpGravityInfluence))
		return;
	FGravityStack* TmpStack{MapGravityInfluences.Find(TmpGravityInfluence->GetPriority())};
	if (!TmpStack)
		return;
	TmpStack->RemoveFromStack(TmpGravityInfluence);
	MapGravityInfluences.KeySort([](const int32 A, const int32 B)
	{
		return A > B; // sort keys in reverse
	});
}
