﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HealerComponent.h"

#include "DrawDebugHelpers.h"
#include "GGJ2022_Duality/Interactables/Wound.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values for this component's properties
UHealerComponent::UHealerComponent() :
	HealRadius{500}
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UHealerComponent::HealWounds() const
{
	const TArray<AActor*> ActorsToIgnore{GetOwner()};
	TArray<FHitResult> WoundsFound;
	const FVector TraceCenter{GetOwner()->GetActorLocation()};
	UKismetSystemLibrary::SphereTraceMulti(this, TraceCenter, TraceCenter, HealRadius,
	                                       UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel2), false,
	                                       ActorsToIgnore, EDrawDebugTrace::None, WoundsFound, true);

	for (int32 Index = 0; Index < WoundsFound.Num(); ++Index)
	{
		const auto& PossibleWound = WoundsFound[Index];
		FHitResult HitWound;
		UKismetSystemLibrary::LineTraceSingle(this, TraceCenter,
		                                      TraceCenter + (PossibleWound.ImpactPoint - TraceCenter).GetSafeNormal() *
		                                      HealRadius, UEngineTypes::ConvertToTraceType(ECC_Visibility), false,
		                                      ActorsToIgnore, EDrawDebugTrace::None, HitWound, true);
		
		auto* Wound = Cast<AWound>(HitWound.Actor);
		if (IsValid(Wound) && !Wound->IsHealed())
		{
			Wound->HealWound();
		}
	}
}
