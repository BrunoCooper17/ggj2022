﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealerComponent.generated.h"

class AWound;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class GGJ2022_DUALITY_API UHealerComponent final : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHealerComponent();

protected:
	UPROPERTY(EditAnywhere, meta=(ClampMin= 1))
	int32 HealRadius;

public:
	UFUNCTION(BlueprintCallable)
	void HealWounds() const;
};
