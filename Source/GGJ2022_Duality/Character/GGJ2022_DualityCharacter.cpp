// Copyright Epic Games, Inc. All Rights Reserved.

#include "GGJ2022_DualityCharacter.h"

#include "GravityControllerComponent.h"
#include "GravityMovementComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "HealerComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GGJ2022_Duality/GameModes/GGJ2022_DualityGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

//////////////////////////////////////////////////////////////////////////
// AGGJ2022_DualityCharacter

AGGJ2022_DualityCharacter::AGGJ2022_DualityCharacter(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	CurrentPlayerController{nullptr},
	CurrentGravity{0},
	bUsingGamepad{false},
	bIsSinging{false},
	GravityInterpSpeed{10.0f}
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 135.f;
	BaseLookUpRate = 135.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(320.0f, 320.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = false; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
	GravityControllerComponent = CreateDefaultSubobject<UGravityControllerComponent>(TEXT("GravityController"));
	HealerComponent = CreateDefaultSubobject<UHealerComponent>(TEXT("Healer"));
}

//////////////////////////////////////////////////////////////////////////
// Input

void AGGJ2022_DualityCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Heal", IE_Pressed, this, &AGGJ2022_DualityCharacter::SingAndHeal);

	PlayerInputComponent->BindAxis("MoveForward", this, &AGGJ2022_DualityCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGGJ2022_DualityCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &AGGJ2022_DualityCharacter::UpdateTurnDummy);
	PlayerInputComponent->BindAxis("TurnRate", this, &AGGJ2022_DualityCharacter::UpdateTurnDummy);
	PlayerInputComponent->BindAxis("LookUp", this, &AGGJ2022_DualityCharacter::UpdateTurnDummy);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AGGJ2022_DualityCharacter::UpdateTurnDummy);

	auto& BindAction{
		InputComponent->BindAction("AnyKey", IE_Pressed, this,
		                           &AGGJ2022_DualityCharacter::CheckInputDeviceType)
	};
	BindAction.bConsumeInput = false;
}

void AGGJ2022_DualityCharacter::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CurrentGravity = GravityControllerComponent->GetGravityDirection();
	UpdateGravityInterpolate(DeltaSeconds);
	UpdateCameraOrientation(DeltaSeconds);
}

void AGGJ2022_DualityCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(GravityControllerComponent,
	                                                          &UGravityControllerComponent::RegisterGravityZone);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(GravityControllerComponent,
	                                                        &UGravityControllerComponent::UnregisterGravityZone);
}

void AGGJ2022_DualityCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	CurrentPlayerController = Cast<APlayerController>(NewController);
}

void AGGJ2022_DualityCharacter::CheckInputDeviceType(const FKey KeyPressed)
{
	bUsingGamepad = KeyPressed.IsGamepadKey();
}

void AGGJ2022_DualityCharacter::MoveForward(const float Value)
{
	AddMovementInput(GetActorForwardVector(), Value);
}

void AGGJ2022_DualityCharacter::MoveRight(const float Value)
{
	AddMovementInput(GetActorRightVector(), Value);
}

void AGGJ2022_DualityCharacter::SingAndHeal()
{
	if (!Cast<UGravityMovementComponent>(GetMovementComponent())->IsMovingOnGround())
		return;
	bIsSinging = true;
	SingEffect();
	DisableInput(CurrentPlayerController);
	UKismetSystemLibrary::Delay(this, 2.f, FLatentActionInfo(1, FMath::Rand(), TEXT("EndSinging"), this));
	HealerComponent->HealWounds();
	Cast<AGGJ2022_DualityGameMode>(UGameplayStatics::GetGameMode(this))->UpdateWoundsHealCounter();
}

void AGGJ2022_DualityCharacter::EndSinging()
{
	bIsSinging = false;
	EnableInput(CurrentPlayerController);
}

void AGGJ2022_DualityCharacter::UpdateCameraOrientation(const float DeltaSeconds)
{
	const float Turn{GetInputAxisValue(bUsingGamepad ? "TurnRate" : "Turn")};
	const float LookUp{-GetInputAxisValue(bUsingGamepad ? "LookUpRate" : "LookUp")};
	const float TurnRate{bUsingGamepad ? BaseTurnRate * DeltaSeconds : 1.f};
	const float LookUpRate{bUsingGamepad ? BaseLookUpRate * DeltaSeconds : 1.f};

	CameraBoom->AddRelativeRotation(FRotator(LookUpRate * LookUp, 0.f, 0.f));
	AddActorLocalRotation(FRotator(0, TurnRate * Turn, 0));
}

void AGGJ2022_DualityCharacter::UpdateGravityInterpolate(const float DeltaSeconds) const
{
	UGravityMovementComponent* GravityComponent = GetGravityMovementComponent();
	const FVector NewGravityDirection{
		UKismetMathLibrary::VInterpTo(GravityComponent->GetGravityDirection(), CurrentGravity,
		                              UGameplayStatics::GetWorldDeltaSeconds(this), GravityInterpSpeed)
	};
	GravityComponent->SetGravityDirection(NewGravityDirection);
}
