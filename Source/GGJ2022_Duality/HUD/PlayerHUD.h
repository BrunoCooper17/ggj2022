// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

class UBaseHudWidget;

/**
 * 
 */
UCLASS()
class GGJ2022_DUALITY_API APlayerHUD final : public AHUD
{
	GENERATED_BODY()
	
public:
	APlayerHUD();

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "True"))
	TSubclassOf<UBaseHudWidget> HudWidget;
	UPROPERTY()
	UBaseHudWidget* HudWidget_Ref;
	
protected:
	virtual void BeginPlay() override;

public:
	void DisplayHud() const;
	void HideHud() const;
	void UpdateTime(float Percentage) const;
	void UpdateWoundCounter(const int32 WoundsLeft) const;
};
