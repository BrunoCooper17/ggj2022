// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"
#include "BaseHudWidget.h"

DECLARE_LOG_CATEGORY_CLASS(LogPlayerHUD, All, All)

APlayerHUD::APlayerHUD() :
	HudWidget{nullptr},
	HudWidget_Ref{nullptr}
{
}

void APlayerHUD::BeginPlay()
{
	Super::BeginPlay();

	if (!HudWidget)
	{
		UE_LOG(LogPlayerHUD, Error, TEXT("No HudWidget to display provided"));
		return;
	}
	HudWidget_Ref = CreateWidget<UBaseHudWidget>(GetOwningPlayerController(), HudWidget);
}

void APlayerHUD::DisplayHud() const
{
	if (IsValid(HudWidget_Ref))
		HudWidget_Ref->Show();
}

void APlayerHUD::HideHud() const
{
	if (IsValid(HudWidget_Ref))
		HudWidget_Ref->Hide();
}

void APlayerHUD::UpdateTime(const float Percentage) const
{
	if (IsValid(HudWidget_Ref))
		HudWidget_Ref->UpdateTimeDisplay(Percentage);
}

void APlayerHUD::UpdateWoundCounter(const int32 WoundsLeft) const
{
	if (IsValid(HudWidget_Ref))
		HudWidget_Ref->UpdateWoundsLeftDisplay(WoundsLeft);
}
