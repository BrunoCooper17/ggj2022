// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseUserWidget.h"
#include "BaseHudWidget.generated.h"

/**
 * 
 */
UCLASS()
class GGJ2022_DUALITY_API UBaseHudWidget final : public UBaseUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateTimeDisplay(float Percentage);
	
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateWoundsLeftDisplay(int32 WoundCount);
};
