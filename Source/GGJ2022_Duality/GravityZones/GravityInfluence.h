// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBase.h"
#include "GravityInfluence.generated.h"

/**
 * 
 */
UCLASS(ClassGroup=Common, abstract, ConversionRoot, MinimalAPI)
class AGravityInfluence : public ATriggerBase
{
	GENERATED_BODY()

public:
	explicit AGravityInfluence(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual FVector GetGravity(const FVector ActorLocation) const { return FVector{0}; }

	int8 GetPriority() const { return Priority; }

protected:
	UPROPERTY(EditAnywhere)
	bool bAttract;

	UPROPERTY(EditAnywhere)
	int8 Priority;
};
