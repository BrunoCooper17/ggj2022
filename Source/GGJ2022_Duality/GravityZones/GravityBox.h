// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GravityInfluence.h"
#include "GravityBox.generated.h"

class UArrowComponent;

/**
 * 
 */
UCLASS()
class AGravityBox : public AGravityInfluence
{
	GENERATED_BODY()

	explicit AGravityBox(const FObjectInitializer& ObjectInitializer);

private:
	UPROPERTY()
	UArrowComponent* ArrowComponent;

public:
	virtual FVector GetGravity(const FVector ActorLocation) const override { return -GetActorUpVector(); }
	virtual void OnConstruction(const FTransform& Transform) override;
};
