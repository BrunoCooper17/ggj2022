// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GravityInfluence.h"
#include "GravitySphere.generated.h"

/**
 * 
 */
UCLASS()
class AGravitySphere : public AGravityInfluence
{
	GENERATED_BODY()

	explicit AGravitySphere(const FObjectInitializer& ObjectInitializer);

public:
	virtual FVector GetGravity(const FVector ActorLocation) const override;
};
