// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GravityInfluence.h"
#include "GravityRamp.generated.h"

class USplineComponent;
class UArrowComponent;

/**
 * 
 */
UCLASS()
class AGravityRamp : public AGravityInfluence
{
	GENERATED_BODY()

	explicit AGravityRamp(const FObjectInitializer& ObjectInitializer);

protected:
	UPROPERTY(EditAnywhere, meta=(ClampMin=100))
	int32 Distance;

private:
	UPROPERTY(EditAnywhere)
	USplineComponent* Reference;

	UPROPERTY()
	UArrowComponent* ArrowComponent;
	
public:
	virtual FVector GetGravity(const FVector ActorLocation) const override;
	virtual void OnConstruction(const FTransform& Transform) override;
};
