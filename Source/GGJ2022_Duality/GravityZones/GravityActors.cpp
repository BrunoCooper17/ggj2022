// Fill out your copyright notice in the Description page of Project Settings.


#include "GravitySphere.h"
#include "GravityBox.h"
#include "GravityRamp.h"
#include "Components/ArrowComponent.h"

#include "Components/BillboardComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SplineComponent.h"
#include "Kismet/KismetMathLibrary.h"

AGravitySphere::AGravitySphere(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer.SetDefaultSubobjectClass<USphereComponent>(TEXT("CollisionComp")))
{
	USphereComponent* SphereCollisionComponent = CastChecked<USphereComponent>(GetCollisionComponent());

	SphereCollisionComponent->ShapeColor = FColor(100, 255, 100, 255);
	SphereCollisionComponent->InitSphereRadius(40.0f);
	SphereCollisionComponent->SetCollisionProfileName(TEXT("TRIGGER"));
#if WITH_EDITORONLY_DATA
	if (UBillboardComponent* TriggerSpriteComponent = GetSpriteComponent())
	{
		TriggerSpriteComponent->SetupAttachment(SphereCollisionComponent);
	}
#endif
}

FVector AGravitySphere::GetGravity(const FVector ActorLocation) const
{
	return UKismetMathLibrary::GetDirectionUnitVector(ActorLocation, GetActorLocation()) * (bAttract ? 1 : -1);
}

AGravityBox::AGravityBox(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer.SetDefaultSubobjectClass<UBoxComponent>(TEXT("CollisionComp")))
{
	UBoxComponent* BoxCollisionComponent = CastChecked<UBoxComponent>(GetCollisionComponent());

	BoxCollisionComponent->ShapeColor = FColor(100, 255, 100, 255);
	BoxCollisionComponent->InitBoxExtent(FVector(40.0f, 40.0f, 40.0f));
	BoxCollisionComponent->SetCollisionProfileName(TEXT("TRIGGER"));
#if WITH_EDITORONLY_DATA
	if (UBillboardComponent* TriggerSpriteComponent = GetSpriteComponent())
	{
		TriggerSpriteComponent->SetupAttachment(BoxCollisionComponent);
	}
#endif
	
	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("GravityDirection"));
	ArrowComponent->SetRelativeRotation((-GetActorUpVector()).Rotation());
	ArrowComponent->SetRelativeScale3D(FVector(1.f, 1.f, 1.f));
	ArrowComponent->ArrowLength = 40;
	ArrowComponent->SetupAttachment(BoxCollisionComponent);
}

void AGravityBox::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	ArrowComponent->ArrowSize = bAttract ? 1 : -1;
}

AGravityRamp::AGravityRamp(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer.SetDefaultSubobjectClass<UBoxComponent>(TEXT("CollisionComp"))),
	Distance{200}
{
	UBoxComponent* BoxCollisionComponent = CastChecked<UBoxComponent>(GetCollisionComponent());

	BoxCollisionComponent->ShapeColor = FColor(100, 255, 100, 255);
	BoxCollisionComponent->InitBoxExtent(FVector(40.f, 40.f, 40.f));
	BoxCollisionComponent->SetCollisionProfileName(TEXT("TRIGGER"));

	Reference = CreateDefaultSubobject<USplineComponent>(TEXT("GravityReference"));
	Reference->SetupAttachment(BoxCollisionComponent);

#if WITH_EDITORONLY_DATA
	if (UBillboardComponent* TriggerSpriteComponent = GetSpriteComponent())
	{
		TriggerSpriteComponent->SetupAttachment(BoxCollisionComponent);
	}
#endif
	
	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("GravityDirection"));
	ArrowComponent->SetRelativeRotation((-GetActorUpVector()).Rotation());
	ArrowComponent->SetRelativeScale3D(FVector(1.f, 8.f, 1.f));
	ArrowComponent->ArrowLength = 40;
	ArrowComponent->SetupAttachment(BoxCollisionComponent);
}

void AGravityRamp::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	const UBoxComponent* BoxCollisionComponent = CastChecked<UBoxComponent>(GetCollisionComponent());
	Reference->SetLocationAtSplinePoint(0, FVector(0, -BoxCollisionComponent->GetScaledBoxExtent().Y, 0),
										ESplineCoordinateSpace::Local, false);
	Reference->SetLocationAtSplinePoint(1, FVector(0, BoxCollisionComponent->GetScaledBoxExtent().Y, 0),
										ESplineCoordinateSpace::Local, false);
	Reference->UpdateSpline();

	Reference->SetRelativeLocation(FVector::UpVector * Distance * (bAttract ? -1 : 1));
}

FVector AGravityRamp::GetGravity(const FVector ActorLocation) const
{
	const FVector NewReference = Reference->FindLocationClosestToWorldLocation(
		ActorLocation, ESplineCoordinateSpace::World);
	return UKismetMathLibrary::GetDirectionUnitVector(ActorLocation, NewReference) * (bAttract ? 1 : -1);
}
