// Copyright 2019 Tefel. All Rights Reserved

#include "GravityCharacter.h"
#include "GravityMovementComponent.h"

AGravityCharacter::AGravityCharacter(const FObjectInitializer& ObjectInitializer) 
		: Super(ObjectInitializer.SetDefaultSubobjectClass<UGravityMovementComponent>(ACharacter::CharacterMovementComponentName))
{
}

UGravityMovementComponent* AGravityCharacter::GetGravityMovementComponent() const
{
	return Cast<UGravityMovementComponent>(GetMovementComponent());
}
